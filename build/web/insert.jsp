<%-- 
    Document   : insert
    Created on : 18 nov. 2022, 13:50:41
    Author     : 26132
--%>
<%@page import="Classes.Genre"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%
    HashMap<String, Object> object = (HashMap<String, Object>) request.getAttribute("donnees");


%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="../../Etudiant/new/.do" method="post">
            <p>Nom :<input type="text" name="nom"></p>
            <p>Prenom :<input type="text" name="prenom"></p>

            <p>Date de naissance : <input type="date" name="dateNaissance"></p>
            <p>Genre : <select name="idgenre">
                    <%for (String key : object.keySet()) {
                        int taille = ((ArrayList<Genre>) object.get(key)).size();
                        for (int i = 0; i < taille; i++) {%>
                    <option value="<%=((ArrayList<Genre>) object.get(key)).get(i).getIdGenre()%>">
                        <%=((ArrayList<Genre>) object.get(key)).get(i).getGenre()%>
                    </option>
                    <% }
                        }%>
                </select>
            </p>
            <p><input type="submit" value="Creer"></p>
        </form>
    </body>
</html>
