<%-- 
    Document   : liste
    Created on : 7 nov. 2022, 11:50:56
    Author     : 26132
--%>
<%@page import="java.util.HashMap"%>
<%@page import="Classes.Etudiant"%>
<%@page import="Classes.Genre"%>
<%@page import="java.util.ArrayList"%>
<%
    HashMap<String, Object> object = (HashMap<String, Object>) request.getAttribute("donnees");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
            <table border="1">
            <th>Nom</th>
            <th>Prenom</th>
            <th>Date de naissance</th>
            <th>Genre</th>


            <%
                for (String key : object.keySet()) {
                    int taille = ((ArrayList<Etudiant>) object.get(key)).size();
                    for (int i = 0; i < taille; i++) {%>
            <tr>
                <td><%=((ArrayList<Etudiant>) object.get(key)).get(i).getNom()%></td>
                <td><%=((ArrayList<Etudiant>) object.get(key)).get(i).getPrenom()%></td>
                <td><%=((ArrayList<Etudiant>) object.get(key)).get(i).getDateNaissance()%></td>
                <td><%=((ArrayList<Etudiant>) object.get(key)).get(i).getIdgenre() %></td>

            </tr>
            <% }
                }
            %>

        </table>
    </body>
</html>
