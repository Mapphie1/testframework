/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Annotations.UrlAnnotation;
import Util.ModelView;
import Util.Utilitaire;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author 26132
 */
public class Genre {

    String IdGenre;
    String Genre;

    public Genre() {
    }

    public Genre(String Genre) {
        this.Genre = Genre;
    }

    public String getIdGenre() {
        return IdGenre;
    }

    public void setIdGenre(String idGenre) {
        this.IdGenre = idGenre;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String Genre) {
        this.Genre = Genre;
    }

    @UrlAnnotation(url = "insertEtudiant")
    public ModelView getAllGenre() throws Exception {
        ModelView mv = new ModelView();
        ArrayList<Genre> genres = new ArrayList<Genre>();
        Utilitaire util = new Utilitaire();
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = util.Connect();
            String sql = "select * from genre";
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();
            while (res.next()) {
                Genre g = new Genre();
                g.setIdGenre(res.getString(1));
                g.setGenre(res.getString(2));
                genres.add(g);
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (res != null) {
                res.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("genre", genres);

        mv.setUrl("/insert.jsp");
        mv.setListe(data);
        return mv;
    }

}
