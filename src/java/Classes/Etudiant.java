/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import Annotations.UrlAnnotation;
import Util.ModelView;
import Util.Utilitaire;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;


/**
 *
 * @author 26132
 */
public class Etudiant {
    String idEtudiant;
    String nom;
    String prenom;
    Date dateNaissance;
    String idgenre;

    public Etudiant(String nom, String prenom, Date dateNaissance,String genre) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.idgenre = genre;
    }

    public Etudiant() {
    }
    

    public String getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(String idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getIdgenre() {
        return idgenre;
    }

    public void setIdgenre(String idgenre) {
        this.idgenre = idgenre;
    }


    
    
    
    @UrlAnnotation(url="lister")
    public ModelView getAllEtudiant()throws Exception {
        ModelView mv = new ModelView();
        ArrayList<Etudiant> etudiants = new ArrayList<Etudiant>();
        Utilitaire util = new Utilitaire();
        ResultSet res = null;
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con =  util.Connect();
            String sql = "select * from etudiant_info";
            stmt = con.prepareStatement(sql);
            res = stmt.executeQuery();
            while(res.next()){
                Etudiant e = new Etudiant();
                e.setIdEtudiant(res.getString(1));
                e.setNom(res.getString(2));
                e.setPrenom(res.getString(3));
                e.setDateNaissance(res.getDate(4));
                e.setIdgenre(res.getString(6));
                etudiants.add(e);
            }
        }
        catch (Exception ex) {
            throw ex;
        } finally {
            if (res != null) {
                res.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("listeEtudiant", etudiants);

        mv.setUrl("/liste.jsp");
        mv.setListe(data);
        return mv;
    }
    
    @UrlAnnotation(url="new")
    public ModelView insert() throws Exception {
        ModelView mv = new ModelView();
        Utilitaire util = new Utilitaire();
        Connection con = null;
        PreparedStatement stmt = null;
        try{
            con = util.Connect();
            stmt = con.prepareStatement("insert into etudiant values(nextval('s_etudiant'),?,?,?,?)");
            stmt.setString(1, this.getNom());
            stmt.setString(2, this.getPrenom());
            stmt.setDate(3, this.getDateNaissance());
            stmt.setString(4, this.idgenre);
            
            stmt.execute();
        }
        catch(Exception e){
            throw e;
        }
        finally {
            
            if (stmt != null) {
                stmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        mv.setUrl("/Etudiant/lister/.do");
        return mv;
    }
}
