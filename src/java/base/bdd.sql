/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ETU 1509 RAZAFINDRAKOTOSON MAPHIE SAROBIDY P14_A N°137
 * Created: 18 nov. 2022
 */

create database framework;
create role tp login password 'tp';
alter database framework owner to tp;

create table Genre(
idgenre varchar(1) primary key,
genre varchar(10)
);

create table Etudiant(
idEtudiant varchar(3) primary key,
nom varchar(20),
prenom varchar(30),
dateNaissance date,
idgenre varchar(1),
foreign key(idgenre) references genre(idgenre)
);

create sequence s_etudiant;

insert into genre values('1','Femme'),('2','Homme');

create or replace view etudiant_info as select etudiant.*,genre.genre from etudiant join genre on etudiant.idgenre = genre.idgenre;
